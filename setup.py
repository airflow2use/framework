
from setuptools import setup, find_packages

setup(
    name='airflow2use',
    version='0.2',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An collection of Airflow plugins & extensions',
    long_description=open('README.txt').read(),
    install_requires=[
        'apache-airflow',
    ],
    extras_require={
        'graph': [
            'graphene-django>=3',
        ],
        'model': [
            'django-rest-swagger',
        ],
        'daten': [
            'rest-pandas',
        ],
        'nerve': [
            'rest-pandas',
        ],
    },
    url='https://github.com/airflow2use/framework',
    author='TAYAA Med Amine',
    author_email='tayamino@gmail.com'
)
